package com.example.webmanager

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.app.Service
import androidx.lifecycle.Observer
import androidx.lifecycle.viewmodel.viewModelFactory
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.work.*
import java.util.EnumSet.range
import java.util.concurrent.TimeUnit

class MainActivity : AppCompatActivity(){

    val adapter = MessageAdapter()
    lateinit var rcView: RecyclerView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val btnShowData: Button = findViewById(R.id.btnShowData)
        val serviceClass = MyService::class.java
        val intent = Intent(this, serviceClass)
        var idList = IntArray(100)

        val viewModel = MessageViewModel()
        rcView = findViewById(R.id.rcView)
        rcView.layoutManager = LinearLayoutManager(this)
        rcView.adapter = adapter

        btnShowData.setOnClickListener{
            startService(intent)
            adapter.clear()
            val myWorkRequest = OneTimeWorkRequest.Builder(MyWorker::class.java).
            setInitialDelay(15, TimeUnit.SECONDS).build()
            WorkManager.getInstance().enqueue(myWorkRequest)
            WorkManager.getInstance().getWorkInfoByIdLiveData(myWorkRequest.id).observe(this, Observer {
                workInfo ->
                if ((workInfo != null) && (workInfo.state.isFinished)){
                    idList = workInfo.outputData.getIntArray("keyId")!!
                    for (i in idList){
                        adapter.addMessage(Message(0, i, "$i", ""))
                    }
                }
            })

        }
    }


}