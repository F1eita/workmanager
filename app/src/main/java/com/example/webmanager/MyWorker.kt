package com.example.webmanager

import android.content.Context
import android.text.Layout
import android.util.Log
import androidx.recyclerview.widget.RecyclerView
import androidx.work.Data
import androidx.work.Worker
import androidx.work.WorkerParameters
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class MyWorker(ctx: Context, params: WorkerParameters) : Worker(ctx, params){

    var idList = ArrayList<Int>()
    val viewModel = MessageViewModel()
    val output = Data.Builder()
    override fun doWork(): Result {
        try {
            val retrofitBuilder = Retrofit.Builder().
            addConverterFactory(GsonConverterFactory.create()).
            baseUrl(BASE_URL).build().
            create(APIInterface::class.java)
            val retrofitData = retrofitBuilder.getData()
            retrofitData.enqueue(object: Callback<List<PostsItem>?> {
                override fun onResponse(
                    call: Call<List<PostsItem>?>,
                    response: Response<List<PostsItem>?>
                ) {
                    val responseBody =  response.body()!!
                    for (Posts in responseBody){
                        idList.add(Posts.id)
                    }
                }

                override fun onFailure(call: Call<List<PostsItem>?>, t: Throwable) {
                    Log.d("MessListFragment", "onFailure: " + t.message)
                }
            })

        } catch (ex: Exception) {
            return Result.failure();
        }
        val output = Data.Builder().putIntArray("keyId", idList.toIntArray()).build()
        return Result.success(output)
    }

}