package com.example.webmanager

data class PostsItem(
    val body: String,
    val id: Int,
    val title: String,
    val userId: Int
)