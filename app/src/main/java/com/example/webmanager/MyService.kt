package com.example.webmanager

import android.app.Service
import android.content.Intent
import android.os.Handler
import android.os.IBinder
import android.util.Log
import android.widget.Toast
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


const val BASE_URL = "https://jsonplaceholder.typicode.com/"

class MyService : Service() {


    override fun onBind(intent: Intent): IBinder? {
        throw UnsupportedOperationException("Not yet implemented")
    }

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        Toast.makeText(this, "Service started.", Toast.LENGTH_SHORT).show()

        getData()

        return START_STICKY
    }

    override fun onDestroy() {
        super.onDestroy()
        Toast.makeText(this, "Service destroyed.", Toast.LENGTH_SHORT).show()
    }

    private fun getData() {
        val retrofitBuilder = Retrofit.Builder().
        addConverterFactory(GsonConverterFactory.create()).
        baseUrl(BASE_URL).build().
        create(APIInterface::class.java)
        val retrofitData = retrofitBuilder.getData()
        retrofitData.enqueue(object: Callback<List<PostsItem>?> {
            override fun onResponse(
                call: Call<List<PostsItem>?>,
                response: Response<List<PostsItem>?>
            ) {
                val responseBody =  response.body()!!
                for (Posts in responseBody){
                    Log.d("JSON", "[${Posts}]")
                }
            }

            override fun onFailure(call: Call<List<PostsItem>?>, t: Throwable) {
                Log.d("MessListFragment", "onFailure: " + t.message)
            }
        })

    }
}
