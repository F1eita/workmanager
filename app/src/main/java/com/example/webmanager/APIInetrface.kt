package com.example.webmanager

import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.*

interface APIInterface {

    @GET("posts")
    fun getData(): Call<List<PostsItem>>

    @FormUrlEncoded
    @POST("posts")
    suspend fun pushPost(
        @Field("PostsItem") postsItem: PostsItem
    ): Response<ResponseBody>
}