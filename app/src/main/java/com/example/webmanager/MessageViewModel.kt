package com.example.webmanager

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.work.Data

class MessageViewModel: ViewModel() {
    private val _mess = MutableLiveData<ArrayList<Message>>(ArrayList<Message>())
    val mess: LiveData<ArrayList<Message>> = _mess
    fun addMess(mess: Message){
        _mess.value?.add(mess)
    }
    fun clear(){
        _mess.value?.clear()
    }
}